#!/bin/bash
version=0.02
DBG=0 # debugging mode; DBG=0 → no debugging
NV=0  # use nVidia encoder; NV=0 → don't
# Dependency:  awk, bbswitch
#              not any more: bumblebee

function optirun_check(){
	hash optirun 2>/dev/null || {
	  echo Missing dependency!;
	  echo Cannot run \(-n\) without the bumblebee package installed.;
	  exit 1;
	}
}
function nvidia_status(){
	pacman -Qq bbswitch 2>/dev/null 1>&2 || {
	  echo Missing dependency!;
	  echo Cannot run \(-n\) without the bbswitch package installed.;
	  exit 1;
	}
	if [[ -f /proc/acpi/bbswitch ]]
	then
		[[ $(cat /proc/acpi/bbswitch) == "OFF" ]] && sudo tee /proc/acpi/bbswitch <<< ON
	else
		echo "No bbswitch?"
		exit 3
	fi
}


FROM=0 		# Starting moment (whichever format you like)
TO=''  		# Stopping moment (hh:mm:ss)
br_cent=70 	# Video bitrate scaling factor ( /100)

usemsg="
The script converts videos form the h264 into the h265 format with x265/hevc.
It uses ffmpeg with a medium preset.
Usage:
\t${0##*/} [-d] [-f FROM] [-t TO] [-n brate] input [output]
\n-f FROM   Starting moment (whichever format you like)
-t TO     Stopping moment, not duration (hh:mm:ss)"'\n          '"If not given, it is set to the full lenght of the input.
-n brate  Use the Nvidia hardware acceplaration with h264_nvdec and hevc_nvenc
          'brate' is the bitrate set for the Nvidia video encoder.
          If brate is set to 0 (zero), ${br_cent}% of the original input brate is used.
          Note that the bitrate is only set in case of the -n parameter.
-d        Switch debugging mode on.
\noutput default name:  \${input%.mp4}.mkv"

while getopts f:t:hvn:d OPT; do
    case "$OPT" in
    	f) FROM=$OPTARG;;
    	t) TO=$OPTARG;;
        h) echo -e "$usemsg"; exit 0;;
        v) echo "${0##*/} version 1.2"; exit 0;;
	n) #optirun_check;
	   echo About to use an NVidia encoder; NV=1;
	   brate=${OPTARG,,};brate=${brate%k};;
	d) DBG=1;;
    esac
done
shift $((OPTIND - 1))

[[ $# -eq 0 ]] && { echo -e "$usemsg"; exit 1;}

# Abort if nVidia is needed, but not available.
((NV)) && nvidia_status

input="$1"
output="${input%.???}.mkv"
((DBG)) && DEBUG: \$output=$output
[[ "$input" == "$output" ]] && output='output.mkv'
output="${2-$output}"

# if not declared, new bitrate is set to 70% of the original one
[[ $brate -eq 0 ]] && {
	brate=$(ffprobe "$input" 2>&1|awk '/bitrate/ {print $6}');
	((DBG)) && DEBUG: Input file video bitrate = ${brate};
	brate=$((brate * $br_cent / 100));
}
((DBG)) && DEBUG: Output file video bitrate = ${brate};

[[ -z "$TO" ]] && TO=$(ffmpeg -i "$input" 2>&1 |grep Duration |awk '/Duration/ {print $2}'|cut -d, -f1)
((DBG)) && echo DEBUG:  FROM=$FROM
((DBG)) && echo DEBUG:  TO='  '$TO

[[ -f "$input" ]] && {
	if (($NV)); then
		# load nVidia module if not loaded
		lsmod|grep nvidia || nvidia-modprobe
	#	optirun ffmpeg -hwaccel cuvid -c:v h264_cuvid -i "$input" -ss $FROM  -t "$TO" -c:v hevc_nvenc -b:v ${brate}k -minrate 400k -maxrate 3000k -preset slow -c:a copy "$output"
####		DRM_PRIME=1 ffmpeg -hwaccel cuvid -c:v h264_cuvid -i "$input" -ss $FROM  -t "$TO" -c:v hevc_nvenc -b:v ${brate}k -minrate 400k -maxrate 3000k -preset slow -c:a copy "$output"
		DRM_PRIME=1 ffmpeg -hwaccel_output_format cuda -c:v h264_cuvid -i "$input" -ss $FROM  -t "$TO" -c:v hevc_nvenc -b:v ${brate}k -minrate 400k -maxrate 3000k -preset slow -c:a copy "$output"
	else
		ffmpeg -i "$input" -ss $FROM  -to "$TO" -c:v libx265 -preset medium -c:a copy "$output"
	fi
}
