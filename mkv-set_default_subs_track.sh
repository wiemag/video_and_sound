#!/bin/bash
version=1.00
# Dependencies:  mkvpropedit(mkvtoolnix-cli), ffprobe(ffmpega), grep

ver_msg() { echo -e "\n\t${0##*/}, version $version"; exit;}
ffp() { ffprobe "$1" 2>&1 |grep -E "Audio"\|Stream\|Duration:;}
run_line() {   echo -e "\nRun:\n\t${0##*/} [-l lang] input files\n";}
langs=(eng pol ger ita dan swe nor fin fre hun spa por ukr cze rum gre tur)
inlangs(){ 
  local el pattern="$1";
  for el in "${@:2}";
    do [[ "$el" == "$pattern" ]] && return 0;
  done; return 1;
}
usage() {
echo -e "\nAn mkvpropedit wrapper script."
echo It sets a chosen subtitle track of input.mkv default.
run_line
  cat <<MESSAGE
-l language
      Use three letter language code (eng, ger, pol, ...).
      Defaults to "pol".
-h    Print this usage message.
-v    Prints the script version.

General example of mkvpropedit usage;
mkvpropedit \\
  --edit track:a1 --set flag-default=0 \ # audio
  --edit track:a2 --set flag-default=1 \ # audio
  --edit track:s1 --set flag-default=0 \ # subtitle
  --edit track:s2 --set flag-default=1   # subtitle
MESSAGE
exit
}

[[ "$1" == '--help' || "$1" == '-h' ]] && usage
[[ "$1" == '--version' || "$1" == '-v' ]] && ver_msg
LANG='pol'	# Language
WARN=0 		# Warnings
[[ "$1" == '-l' ]] && {
  LANG="$2";
#  [[ " ${langs[@]} " =~ " ${LANG} " ]] || {
  inlangs "$LANG"  ${langs[@]} || {
	echo -e "\nWarning!";
	echo -e "Language '$LANG' is not in the script language list.\n";
	WARN=1
  }
  shift 2;
}

for input in "$@"; do
  echo input=$input
pierwszy=$(ffp "$input"|grep -m1 'Subtitle' |grep -oP '(?<=:)[0-9]+(?=\()')
def_old=$(ffp "$input" |grep -m1 'Subtitle.*default' |grep -oP '(?<=:)[0-9]+(?=\()')
def_old=$((def_old - pierwszy + 1))
def_new=$(ffp "$input" |grep '('"$LANG"').*Subtitle' |grep -oP '(?<=:)[0-9]+(?=\()')
def_new=$((def_new - pierwszy + 1))
WARN=1
((WARN)) && {
  echo '  'First sub number   = $pierwszy 1>&2;
  echo '  'Old_default number = $def_old 1>&2;
  echo '  'New_default number = $def_new 1>&2;
}

(( def_new >= 1 )) && {
(( def_old == def_new )) || {
  (( def_old < 1 )) && 
    mkvpropedit "$input" --edit track:s"$def_new" --set flag-default=1 ||
    mkvpropedit "$input" \
      --edit track:s"$def_old" --set flag-default=0 \
      --edit track:s"$def_new" --set flag-default=1;
}
} || {
  echo Language not found in \'"$input"\'.
  echo Aborting.;
  ffp "$input";
  echo Verify the language and re-run the script.;
  exit 6
}
done
