#!/bin/bash
VERSION=1.0
# Script to convert m4b (audiobook) files with embedded chapters (e.g. converted from Audbile) into individual chapter files
# Author:  Eloise Speight
# https://unix.stackexchange.com/users/376235/eloise-speigh
# https://unix.stackexchange.com/questions/499179/using-ffmpeg-to-split-an-audible-audio-book-into-chapters
# Wrapped by wm (help, dependencies, minor modifications)

function help_msg(){
echo -e "\nThe script converts an m4b (audiobook) file with embedded"
echo "chapter information (e.g. converted from Audbile)"
echo -e "into individual chapter files.\n"
echo -e "Run:\n\t${0##*/} [-c][-o] input.m4b [output_type] | --help | -h | -v\n"
echo -e "Output types: mp3, m4a, m4b (default)"
echo '-c = rm all supplementary files (.dat, .jpg,...) at the end'
echo '-o = overwrite the .dat file if it exists'
echo '     (.dat holds info about chapters)'
echo '-h = show this help message'
echo '-v = show script version number'
}
CLEAN=0
OWRITE=0
while getopts "cohv" flag
do
    case "$flag" in
	c) CLEAN=1;;
	o) OWRITE=1;;
	h) help_msg; exit;;
    v) echo -e "\n${0##*/}, version ${VERSION}" && exit;;
    esac
done
# Remove the options parsed above.
shift $(expr $OPTIND - 1)

# display help message or discover the file type (extension) of the input file
input=${1-}
ext=${input##*.}
if [[ -z $ext || $ext == '--help' ]]; then
	help_msg
	exit
fi
if [[ $(file -b "$input" |grep -o 'MP4 .*v1') != 'MP4 Base Media v1' ]]; then
	echo -e "\nError. Command 'file' reports that"
	echo \'$input\' is not an m4b file.
	exit 1
fi
echo "extension: $ext"

# Dependencies: ffmpeg, atomicparsley, jq, sed
function checkdeps(){
	local f
	f="$1"
	hash $f 2>/dev/null ||
	{ echo Missing dependency: ${f,,}; exit 1;}
}
checkdeps AtomicParsley  # atomicparsley (embeds pictures & adds additional metadata to m4a/m4b AAC files)
checkdeps jq             # jq (json interpreter)
checkdeps ffmpeg

# all files / folders are named based on the "shortname" of the input file
shortname=$(basename "$input" ".$ext")
picture="$shortname.jpg"
chapterdata="$shortname.dat"
metadata="$shortname.tmp"
echo "shortname: $shortname"

picmap=$(ffprobe "$input" 2>&1|grep Stream.*Video|grep -o \#\[0-9\]:\[0-9\])
ffmpeg -i "$input" -map ${picmap#\#} -c copy "$picture"
if [[ ! -e "$picture" ]]; then
	exit 111
fi

# if an output type has been given on the command line, set parameters (used in ffmpeg command later)
outputtype=$2
if [[ $outputtype = "mp3" ]]; then
  codec="libmp3lame"
elif [[ $outputtype = "m4a" ]]; then
  codec="copy"
else
  outputtype="m4b"
  codec="copy"
fi
echo "outputtype: $outputtype"

# if it doesn't already exist, create a json file containing the chapter breaks (you can edit this file if you want chapters to be named rather than simply "Chapter 1", etc that Audible use)
[[ $OWRITE -eq 1 ]] && [[ -e "$chapterdata" ]] && rm "$chapterdata"
[ ! -e "$chapterdata" ] && ffprobe -loglevel error \
    -i "$input" -print_format json -show_chapters -loglevel error -sexagesimal \
    >"$chapterdata"
read -p "Now edit file $chapterdata if required. Press ENTER to continue."
# comment out above if you don't want the script to pause!

# read the chapters into arrays for later processing
readarray -t id <<< $(jq -r '.chapters[].id' "$chapterdata")
readarray -t start <<< $(jq -r '.chapters[].start_time' "$chapterdata")
readarray -t end <<< $(jq -r '.chapters[].end_time' "$chapterdata")
readarray -t title <<< $(jq -r '.chapters[].tags.title' "$chapterdata")

# create an ffmpeg metadata file to extract addition metadata lost in splitting files - deleted afterwards
ffmpeg -loglevel error -i "$input" -f ffmetadata "$metadata"
artist_sort=$(sed 's/.*=\(.*\)/\1/' <<<$(cat "$metadata" |grep -m 1 ^sort_artist))
album_sort=$(sed 's/.*=\(.*\)/\1/' <<<$(cat "$metadata" |grep -m 1 ^sort_album))

# create directory for the output
mkdir -p "$shortname"
echo -e "\fID\tStart Time\tEnd Time\tTitle\t\tFilename"
for i in ${!id[@]}; do
  let trackno=$i+1
  # set the name for output - currently in format <bookname>/<tranck number>
  outname="$shortname/$(printf "%02d" $trackno). $shortname - ${title[$i]}".$outputtype
  outname=$(sed 's/:/_/g' <<< $outname)
  echo -e "${id[$i]}\t${start[$i]}\t${end[$i]}\t${title[$i]}\n\t\t$(basename "$outname")"
  ffmpeg -loglevel error -i "$input" -vn -c $codec \
            -ss ${start[$i]} -to ${end[$i]} \
            -metadata title="${title[$i]}" \
            -metadata track=$trackno \
            -map_metadata 0 -id3v2_version 3 \
            "$outname"
  [[ $outputtype =~ m4* ]] && AtomicParsley "$outname" \
            --artwork "$picture" --overWrite \
            --sortOrder artist "$artist_sort" \
            --sortOrder album "$album_sort" \
            > /dev/null
done

# Clean up
[[ $CLEAN -eq 1 ]] && rm "$picture" "$metadata" "$chapterdata"
