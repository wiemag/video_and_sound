#!/bin/bash
version=0.02
SUSPEND=0

Usage() {
	echo -e "\nUsage:\n\t${0##*/} [-s] <INPUT.mp4 files> | -h\n" 1>&2;
	echo '-s suspends computer at the end' 1>&2;
	echo -e "-h prints this help\n" 1>&2;
	echo "The script converts MP4's (h264) into *.mkv (h265 codec)." 1>&2;
	echo 'It is a h264-2-h265.sh wrapper.' 1>&2;
	exit;
}

while getopts ":sh" opt; do
  case $opt in
    s) SUSPEND=1;;
    h) Usage;;
  esac
done
shift $((OPTIND-1))

# Check if file is MP4 type (see the 'file' command below).
for f in "$@"; do
	if [[ -r "$f" ]]; then
		[[ -z $(file -b --mime-type "$f"|grep -q mp4) ]] &&
		( h264-2-h265.sh "$f" && shred -u -n1 -z -s 10K "$f";) ||
		echo \'$f\' is not an MP4 file
	else
		echo  \'$f\' does not exist or is not readable.
	fi
done && ( [[ $SUSPEND -eq 1 ]] && systemctl suspend;)

#https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
