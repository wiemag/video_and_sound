#!/bin/bash
version=0.02
for f in "$@"; do
	echo $f
	if [[ "$(file -b --mime-type "$f")" == 'video/mp4' ]]
	then
		br=$(ffprobe 2>&1 "$f"|grep 'Duration.*bitrate'|sed 's/.*: //')
		br=${br% *}
		if [[ $br -gt 750 ]]; then
			# -hide_banner -log_level level -nostats
			# level =
			# quiet, panic, fatal, error, warning, info, verbose, debug
			# -v 0 disables progress output
			h264-2-h265.sh -n 700 "$f" && echo $br
		else
			ffmpeg -i "$f" -c copy "${f%.mp4}.mkv"
			echo Bitrate $br
		fi
	else
		echo -e "$f\nNot an MP4 file. Skipping."
	fi
done
