#!/bin/bash

function usage(){
	echo -e "Run:\n\t${0##*/} 10bit_input.mkv [8bit_mkv_output_name]\n"
	echo "Output defaults to 'output.mkv'"
}

function nv_bbswitch_ON(){
if [[ -f /proc/acpi/bbswitch ]]
then
	if [[ $(cat /proc/acpi/bbswitch|cut -d\  -f2) != 'ON' ]]
	then
    	sudo tee /proc/acpi/bbswitch <<<ON || exit 3
    fi
else
	echo Missing bbswitch. Install it or check what\'s happened.
	exit 4
fi
}

# Input file
input="${1-}"
[[ -n "$input" ]] || { usage; exit 1;}
ffprobe -hide_banner "$input" 2>&1|grep -q Main\ 10 ||
	{ echo Not a 10bit movie; exit 5;}
# Output file
output="${2-output}.mkv"
# Load nvidia modules if not loaded yet
lsmod|grep -q nvidia || { sudo modprobe nvidia_drm || exit 2;}
# Switch nvidia on if not switched on yet
nv_bbswitch_ON

# Convert
DRM_PRIME=1 ffmpeg -y -hide_banner \
	-c:v hevc_cuvid \
	-i "$input" \
	-c:v hevc_nvenc -minrate 400k -maxrate 3000k \
	-preset fast \
	-pix_fmt yuv420p \
	-c:a copy \
	-c:s copy \
	"$output"
	#-preset slow \
