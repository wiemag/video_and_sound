#!/bin/bash
version=0.01
debug=0
#dependencies: gawk, sed, id3v2
echo -e "\nRun:\n\t${0##*/} mp3_file(s)\n"
echo Sometimes it is enough to delete a frame in the mp3, or modify a frame
echo value to convert the mp3 mime-type from octet-stream to audio/mpeg.

(($#)) || { echo -e "\nMissing input file(s)."; exit 1;}

for f in "$@"
do
  tmp="$(mktemp --suffix=.mp3)"
  tag="$(id3v2 -l "$f")"
  a="$(awk -F: '/^TPE1/ {print $2}' <<<$tag |xargs)"
  t="$(awk -F: '/^TIT2/ {print $2}' <<<$tag |xargs)"
  A="$(awk -F: '/^TALB/ {print $2}' <<<$tag |xargs)"
  y="$(awk -F: '/^TYER/ {print $2}' <<<$tag |xargs)"
  [[ -z "$y" ]] || y="-y $y";
  T="$(awk -F: '/^TRCK/ {print $2}' <<<$tag |xargs)"
  [[ -z "$T" ]] || T="-T $T";
  g="$(sed -n '/^TCON/s/^.*(\([0-9]*\).*/\1/p' <<<$tag |xargs)"
  [[ -z "$g" ]] || g="-g $g"

  lyrics="$(sed -n '/USLT/,$p' <<<$tag)"
  lyrics="$(sed -e '1 s/^USLT.*\]: //' <<<$lyrics)"
  lyrics="$(sed -e '/^[A-Z][A-Z][A-Z][A-Z] (/d' <<<$lyrics)"
  lyrics="$(sed -e '/^[A-Z][A-Z][A-Z][0-9] (/d' <<<$lyrics)"
  lyrics="$(sed -e 's/\[/\\\\[/' -e 's/]/\\\\]/' <<<$lyrics)"

  ((debug)) && {
    echo -e "\nFIELDS IDENTIFIED:\n a=$a t=$t A=$A y=$y T=$T g=$g"
    echo -e "\nThe USLT filed (lyrics/text):\n$lyrics"|head -10
    echo -e "\nFFMPEG"
  }  
  ffmpeg -hide_banner -loglevel quiet -y -i "$f" -c copy "$tmp"

  ((debug)) && echo -e "\nID3v2"
  id3v2 -a "$a" -t "$t" -A "$A" $T $y $g --USLT "$lyrics" "$tmp" &&
    mv "$tmp" "$f"
  cat <<<$lyrics
done

# The following command will print the output after removing the spaces from the beginning of the variable, $myVar
#$ echo "Hello ${myVar##*( )}"

#The following command will print the output after removing the spaces from the ending of the variable, $myVar
#$ echo "${myVar%%*( )} is welcome to our site"
#

# Sth to think about: ffmpeg -i audio.mp3 -i logo.png -map 0:0 -map 1:0 -c copy -id3v2_version 3 -metadata:s:v title="Album cover" -metadata:s:v comment="Cover (front)" out.mp3
# Note version 3 id3v2.
