#!/bin/bash

while [[ -f "$1" ]]; do
	MOVIE="$1"
	echo $MOVIE
	echo Trying to set the first subtitle track default...
	# example: mkvpropedit "$MOVIE" --edit track:s1 --set flag-default=0 --edit track:s2 --set flag-default=1
	# set the first subtitle tack default
	mkvpropedit "$MOVIE" --edit track:s1 --set flag-default=1
	shift
done
