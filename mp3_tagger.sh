#!/bin/bash
version="0.24" # August 2024
DRYRUN=0
# Note. The main dependency, i.e. id3v2 does not handle v2.4 id3 tags.
# It seems that the upstream are reluctant to switch from v2.3 up.
hash id3v2 2>/dev/null || { echo Missing dependency: id3v2; exit 9;}
set -u  # Prevent referencing unset variables
debug=0 # Note:  Some functions have their local debug variables (debugLoc).
v2='' # Tag handling: '' (uses v1 and v2), or '-2' (removes 1, uses only v2)

arg="${1:-}"
[[ "x$arg" == 'x-v' ]] && { echo -e "\n${0##*/}, version $version"; exit;}
[[ "x$arg" == 'x-2' ]] && { v2='-2'; shift; arg="${1:-}";}

[[ -z "$arg" || "x$arg" == 'x-h' || "x$arg" == 'x--help' ]] && {
  cat<<INFO

An mp3 tagging script, a id3v2 wrapper.
Written with tagging audiobook mp3's in mind.
Although you can use the script with album songs, as well.

Actions:
     ${0##*/} [-2] -f 'filename format' [options] mp3_file(s)

Help/Info:
     ${0##*/} -hf      # print file format information
     ${0##*/} -l       # list musical genres symbols
     ${0##*/} -h | -v  # print this help or the script version

Option -2 has the highest priority, but must come first in the command line.
          It handles tag versions. If v2 does't exist,
          it makes the script try to convert v1 to v2 first, then it
          removes v1, before applying the other options to v2 only.

Option -f has higher priority than the other "action" options.
  -f format :  The format describes how the the file names are parsed and
               lets the script determine id3 fields for individual files.
If the '-f format' doesn't determine all id3 fields, the other options are used
  -a artist    auduobook writer / singer / band
  -A album     audiobook title / album title
  -t title     short format-like episode/chapter title
               (see option -f: title_format)
               Special use:  -t 'FILENAME'
               Envoking the actual word FILENAME (or filename) will
               make the script use \${f##*-} as the title.
               Still the -f option has a higher priority if the -f option
               uses the 't' token.
  -T track     track number; If there are more than one input file,
               the files are numbered 1, 2,...
  -g genre     integer, e.g. 183=audiobook, 6=grunge, 17=rock
  -y year      integer, empty/null until initialised

-f format and examples:
	The -f format uses tokens in the set defined as (a, A, t, T, g, y, %).
	Any other character is a separator.
	The token meaning as the sam as typical option meanings as described above.
	The '%' token represents a sub-string to be dropped.

	Format 'a_t-T.%' means 'artist' + the '_' separator + 'title' + 
	the '-' separator + 'track no' + the '.' separator + part_to_be_dropped.

(1) title and track numbers determined by the format, artist by the -a option
	${0##*/} -f 't_T.%' -a artist_name *mp3
	The underscore (_) and the dot (.) are separators here.
	The percent (%) is dropped.
(2) title from the filename, artist and track number from the -f format:
	${0##*/} -f 'a-%_T.%' -t FILENAME -A audiobook_title *mp3

INFO
  exit;
}

### VARIABLES ###

# Variables used by functions that parse filenames to determine
# id3v3 fields, like artist, album, title,...
#
declare -ar all_tokens=('a' 'A' 't' 'T' 'y' 'g' '%')
declare -Ar all_fields=(
	[a]='artist'
	[A]='album'
	[t]='title'
	[T]='track'
	[y]='year'
	[g]='genre'
	[%]='dropit'
)
declare -A id3fields=(
	[a]=''
	[A]=''
	[t]=''
	[T]=''
	[y]=''
	[g]=''
	[%]=''
)
declare tokens=() # List of field tokens found in the declared filename format
declare seps=()   # List of separators between the field tokens
# Filename format is a string using "tokens" (a, A, t, T, y, g, %) to refer to
# id3 fields. The extra '%' symbol stands for strings to be dropped,
# and one can use the '%' multiple times in the format.
# a=artist  A=Album/Book  t=title/song/episode/chunk_title
# T=track/episode_no  y=year  g=genre; extra: %="drop this chunk"


# Some mp3's are identified as the 'application/octet-stream' mime-type.
# The author converts them to audio/mpeg, by
# ffmpeg -i "$f" -c copy "{f%mp3}x.mp3" && mv "{f%mp3}x.mp3" "$f"
function isMP3() { [[ $(file -b --mime-type "$1") == 'audio/mpeg' ]];}

function zero_id3fields_array() {
local t
 for t in ${all_tokens[@]}; do id3fields[$t]=''; done
}

function parse_format() {
local char fmt="$1"  # Format of the filename
local debugLoc=0     # Local debugging
  ((debugLoc)) && {
  	>&2 echo fmt=$fmt
  	>&2 echo "    0123456789012345678901234567890"
  }
  local i j=0 k  # declare integers
  # Scan the filename format, using these variables:
  # i=pos.of char, j=pos.of field token found, k=pos.of next filed token
  for ((i=0; i<"${#fmt}"; i++))
  do
    char="${fmt:$i:1}"
    case "$char" in
      'a') tokens+=('a');k=$i;((debugLoc)) && echo a i=$i j=$j;;
      'A') tokens+=('A');k=$i;((debugLoc)) && echo A i=$i j=$j;;
      't') tokens+=('t');k=$i;((debugLoc)) && echo t i=$i j=$j;;
      'y') tokens+=('y');k=$i;((debugLoc)) && echo y i=$i j=$j;;
      'T') tokens+=('T');k=$i;((debugLoc)) && echo T i=$i j=$j;;
      'g') tokens+=('g');k=$i;((debugLoc)) && echo g i=$i j=$j;;
      '%') tokens+=('%');k=$i;((debugLoc)) && echo '%' i=$i j=$j;;
      # Add the total number of songs/episodes per book/album
    esac
    ((k>j)) && {
      seps+=("${fmt:j+1:k-j-1}");
      j=$i; # Save the current pos. in j, and go on looking for the next one
    }
  done
  ((debugLoc)) && { >&2 echo List of field tokens: ${tokens[@]}
    # 'for char in ${seps[@]}' loop can't be used because
    # there can be 'space' separators.
    # Instead, use 'for ((i; i<${#seps[@]}; ++i))'
    >&2 echo Number of separators ${#seps[@]}
    for ((i=0; i<${#seps[@]}; ++i)); do >&2 echo seps\[$i\]: %${seps[$i]}%; done
  }
}

# Determine id3v2 parameters by parsing filename acc.to the filename format.
function parse_filename() {
local debugLoc=0  # Local debugging
local fn="${1%.mp3}" # filename
local i s token sep  # token & separator are used only for easier analysis
  fn="${fn##*/}"
  ((debugLoc)) && >&2 echo "parse_filename():  Filename=$fn"
  for ((i=0; i<${#seps[@]}; ++i)) # token in ${tokens[@]}
  do
    token="${tokens[$i]}"
    sep="${seps[$i]}"
    id3fields[$token]="${fn%%${sep}*}"
    fn="${fn#${id3fields[$token]}${sep}}" # The line order is important here.
    id3fields[$token]="${id3fields[$token]//_/ }"
    ((debugLoc)) && >&2 echo token=${token}: sep=[${sep}] "${id3fields[$token]}"
    # This must be done after new fn is calculated.
    case $token in
      'y'|'g'|'T') id3fields[$token]="-${token} ${id3fields[$token]}";;
    esac
  done
	token="${tokens[$i]}"
	sep='' # An empty separator behind the last token/field.
	id3fields[$token]="${fn//_/ }"
	((debugLoc)) && >&2 echo token=${token}: sep=[${sep}] "${id3fields[$token]}"
}

function fill_in_id3fields() {
local debugLoc=0  # local debugging
local token
  set +u  # In case ${all_fields[$token]} is NULL/empty.
          #+See the declare command below, and the $ref reference further on.
  for token in ${all_tokens[@]}
  do
    declare -n ref="${all_fields[$token]}" # ref becomes a field,
                                           #+as defined by command line flags,
                                           #+and holds its value.
    [[ -z "${id3fields[$token]}" && -n "$ref" ]] && {
      id3fields[$token]="${ref//_/ }";     # The substitution works on values.
      # Tokens y, g, T are integers.
      # Command '((ref)) || ...' makes the field empty if it holds a 0.
      case $token in
        'y'|'g'|'T')
           ((ref)) && \
              id3fields[$token]="-${token} ${id3fields[$token]}" || \
              id3fields[$token]='';;
      esac
    }
    ((debugLoc)) && {
      >&2 printf "%s flag=%s id3f=%s\n" $token "$ref" "${id3fields[$token]}";}
      # declare -p ref # print reference
  done
  set u
}

function list_codes() {
cat<<CODES

00  Blues         11  Oldies          22  Death Metal        78  Rock'n'Roll
01  Classic rock  12  Other           24  Sound Track        80  Folk
02  Country       13  Pop             26  Ambient            83  Swing
03  Dance         14  Rhythm & blues  32  Classical          86  Latin
04  Disco         15  Rap             40  Alternative rock  103  Opera
05  Funk          16  Rock            42  Soul              137  Heavy Metal
06  Grunge        17  Techno          43  Punk              147  Synthpop
07  Hip-hop       18  Reggae          45  Meditative        148  Christmas
08  Jazz          19  Industrial      52  Electronic        182  Neocalssical
09  Metal         20  Alternative     66  New Wave          183  Audiobook
10  New age       21  Ska             67  Psychodelic

See https://en.wikipedia.org/wiki/List_of_ID3v1_genres

CODES
}

function v2_only() {
local f="$1"
local s=$(id3v2 -l "$f" | grep -i id3)
  if grep -q 'id3v1' <<<$s #found
  then
    # Convert id3v1 to id3v2 if id3v3 does not exist.
  	grep -q 'id3v2' <<<$s || id3v2 --convert "$f" >/dev/null
  	# Remove id3v1
  	id3v2 --delete-v1 "$f" > /dev/null
  fi
}

function determine_title() {
local t="$1"
  t="${t//_/ }";
  # Implicite assumption: The title starts after the last dash.
  t="${t##*-}";
  # echo to remove the leading space from the title
  t_opt="$(echo ${t%.mp3})";
  >&2 echo DEBUG: t_opt=$t_opt
}

function trim_string() {
local s="${1:-}"
	if [[ -n "$s" ]]
	then
		s="${s//\'/\\\'}" # in case the string includes an apostrophy
		s="$(echo "$s" | xargs )" # using xargs formating feature
	fi
	echo "$s"
}

function echo_the_action() {
local alb="$1"
  [[ -z "${id3fields[A]}" ]] && alb='' || alb="-A \"${id3fields[A]}\""
  echo id3v2 "$v2" -a \"${id3fields[a]}\" $alb -t \"${id3fields[t]}\" \
    ${id3fields[T]} ${id3fields[y]} ${id3fields[g]} \"$1\"
}

artist=''; album=''; title=''; t_opt=''; T_opt=''
t_format=''  #  format

declare -i genre=0 year=0 track=0 # integers, empty if not initialised
FN=0	# FILENAME based title (1)

# Read all the parameters and use them if the relevant id3fields[] are empty.
# Fill in the empty id3fields[] and use only id3fields in the tagging command.

while getopts "a:A:t:T:y:g:f:dl" arg; do
  case $arg in
    a) artist="$OPTARG";;
    A) album="$OPTARG";;
    t) title="$OPTARG";
       [[ "${title,,}" == 'filename' ]] && FN=1;;
    T) track="$OPTARG";;
    g) genre="$OPTARG";;
    y) year="$OPTARG";;
    f) t_format="$OPTARG";;
    d) debug=1;;
    l) list_codes; exit;;
  esac
done
shift $((OPTIND-1))
number=$# 	# number of mp3's to tag
digits=${#number} # no of digits for track numbers (increased autmat.if needed)

((debug)) && {
	>&2 echo Filename based titles: FN=$FN;
	>&2 echo -e "Number of files: number=$number\n";
}



# Determine the list of id3fields to be parsed from the filename
# if format is a non-zero-length string.
[[ -z "$t_format" ]] || parse_format "$t_format"
((debug)) && >&2 echo -e "The tokens used in the format: ${tokens[@]}"

# Track numbering (num) only if $number>0 and 'album' is determined, i.e.
# $album is non-empty or ${tokens[@]} includes an 'a'.
((number>1)) && [[ -n "$album" || ${tokens[@]} =~ 'A' ]] &&
  { num=1; track=1;} || num=0
((debug)) && {
	>&2 echo -e "numbering=$num\nnumber=$number\nalbum=$album"
	[[ ${tokens[@]} =~ 'A' ]] && >&2 echo 'The A (album) token has been used.'
}

### BEGIN ACTION ###

for f in "$@"
do
  isMP3 "$f" && {
    zero_id3fields_array
    [[ -n "$v2" ]] && v2_only "$f" # strip id3v1 tag, use id3v2 tag only
    # Determine field values, using the naming format if available
    [[ -z "$t_format" ]] || parse_filename "$f"
    # Determine field values, using other flags
    if ((FN))
    then
    	determine_title "$f"
    	id3fields[t]="$t_opt"
    else
      if ((track))
      then
        [[ -z "$title" ]] || title="${title//_/ }"
        ((debug)) && echo Next track = $track >&2
      fi
    fi
    id3fields[t]="$(trim_string "${id3fields[t]}")"
    id3fields[a]="$(trim_string "${id3fields[a]}")"
    id3fields[A]="$(trim_string "${id3fields[A]}")"
    ((debug)) && {
    	echo -e "DEBUG:\ntitle=${title} : id3fields[t]='${id3fields[t]}'"
    	echo -e "artist=${artist}  :  id3fields[a]='${id3fields[a]}'"
    	echo -e "track=${track}  :  id3fields[T]='${id3fields[T]}'"
    }
    # Prepare the identified/determined fields for the writing them in tag(s)
    fill_in_id3fields
        ((num)) && ((++track))
    # Print what is going to be done.
    echo_the_action "$f"
    # genre, year and track without double quotes!
    # They are either empty or hold a spaceless value.
    ((DRYRUN)) || id3v2 $v2 \
    	-a "${id3fields[a]}" \
    	-A "${id3fields[A]}" \
    	-t "${id3fields[t]}" \
    	${id3fields[T]} ${id3fields[y]} ${id3fields[g]} "$f"
  } || >&2 echo Skipping, not an MP3: $f
done
