#!/bin/bash
# https://gist.github.com/Brainiarc7/c6164520f082c27ae7bbea9556d4a3ba
for i in encoders decoders filters; do
    echo $i:; ffmpeg -hide_banner -${i} | egrep -i "npp|cuvid|nvenc|cuda|nvdec"
done

# Also see:
# https://gist.github.com/jbboehr/f487b659cac086b176703c718d797f3b
